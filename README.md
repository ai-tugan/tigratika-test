# Тест проект Tigratika

### ENV
создать .env (из .env.example)

выполнить успешно следующие команды:

    composer install
    ./vendor/bin/sail build --no-cache
    ./vendor/bin/sail up
    ./vendor/bin/sail artisan sail:publish
    ./vendor/bin/sail artisan vendor:publish
    ./vendor/bin/sail artisan cache:clear
    ./vendor/bin/sail artisan config:cache
    ./vendor/bin/sail artisan migrate

открыть страницу проекта [http://0.0.0.0/tablegen](http://0.0.0.0/tablegen)

скачать файл [https://quarta-hunt.ru/bitrix/catalog_export/export_Ngq.xml](https://quarta-hunt.ru/bitrix/catalog_export/export_Ngq.xml) и загрузить в базу.

