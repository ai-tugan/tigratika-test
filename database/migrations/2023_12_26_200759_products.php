<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('offer_id');
            $table->string('name');
            $table->string('url');
            $table->string('picture', 512)->nullable();
            $table->integer('category_id');
            $table->string('currency_id')->nullable();
            $table->integer('price');
            $table->integer('oldprice');
            $table->string('vendor')->nullable();
            $table->boolean('available')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
