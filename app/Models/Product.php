<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'offer_id', 'name', 'url', 'picture', 'category_id', 'currency_id', 'price', 'oldprice', 'vendor', 'available'
    ];

    static public function getProducts()
    {
        return DB::table('products AS p')->leftJoin('category AS c1', 'p.category_id', 'c1.id')
            ->leftJoin('category AS c2', 'c1.parent_id', 'c2.id')
            ->leftJoin('category AS c3', 'c2.parent_id', 'c3.id')
            ->select('p.*',
                DB::raw('CASE WHEN c3.name IS NULL THEN c2.name ELSE c3.name END AS category,
                    CASE WHEN c3.name IS NULL THEN c1.name ELSE c2.name END AS sub_category,
                    CASE WHEN c3.name IS NULL THEN NULL ELSE c1.name END AS sub_sub_category')
            )
            ->orderByRaw('category, sub_category, sub_sub_category')
            ->get();
    }
}
