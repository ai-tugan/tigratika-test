<?php
namespace App\Services;

use Illuminate\Support\Facades\DB;
use Shuchkin\SimpleXLSXGen;
use Illuminate\Support\Facades\Date;
use App\Models\Product;

/**
 *
 * @author art
 *        
 */
class ExcelGenerator
{

    /**
     */
    public function __construct()
    {}

    public function saveExcel(): string|null
    {
        $products = Product::getProducts();
        $data = [[
            '<b>id</b>',
            '<b>категория</b>',
            '<b>подкатегория</b>',
            '<b>субподкатегория</b>',
            '<b>доступен</b>',
            '<b>название</b>',
            '<b>валюта</b>',
            '<b>цена</b>',
            '<b>старая цена</b>',
            '<b>url</b>',
            '<b>picture</b>',
            '<b>вендор</b>'
        ]];
        foreach ($products as $row) {
            $data[] = [
                $row->offer_id,
                $row->category,
                $row->sub_category,
                $row->sub_sub_category,
                ($row->available ? 'да' : 'нет'),
                $row->name,
                $row->currency_id,
                $row->price,
                $row->oldprice,
                $row->url,
                $row->picture,
                $row->vendor
            ];
        }
        array_map('unlink', array_filter((array) glob(public_path('/download/').'*')));
        $fileName = preg_replace('/(\s|-|:)/', '_', 'export_'.Date::now().'.xlsx');
        $exportFile = public_path('/download/').$fileName;
        if (SimpleXLSXGen::fromArray($data)->saveAs($exportFile)) {
            return $fileName;
        } else {
            return null;
        }
    }
}

