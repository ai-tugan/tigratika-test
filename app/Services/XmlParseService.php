<?php
namespace App\Services;

use App\Models\Category;
use App\Models\Product;

/**
 *
 * @author art
 *        
 */
class XmlParseService
{

    /**
     */
    public function __construct()
    {}

    public function parseAndSave(String $xml): bool
    {
        $dom = new \DOMDocument();
        if (! $dom->loadXML($xml)) {
            return false;
        }
        $xpath = new \DOMXPath($dom);
        $category = $xpath->query('//yml_catalog//categories/*');
        if (sizeof($category)) {
            Category::query()->delete();
        }
        foreach ($category as $row) {
            $id = $row->getAttribute('id');
            $parentId = (! empty($row->getAttribute('parentId')) ? $row->getAttribute('parentId') : null);
            $name = $row->nodeValue;
            Category::query()->create([
                'id' => $id,
                'parent_id' => $parentId,
                'name' => $name
            ]);
        }
        $products = $xpath->query('//yml_catalog//offers/*');
        if (sizeof($products)) {
            Product::query()->delete();
        }
        foreach ($products as $row) {
            $offer_id = $row->getAttribute('id');
            $available = ($row->getAttribute('available') == 'true' ? 1 : 0);
            $url = $row->getElementsByTagName('url')[0]->nodeValue;
            $name = $row->getElementsByTagName('name')[0]->nodeValue;
            $picture = ($row->getElementsByTagName('picture')->count() ? $row->getElementsByTagName('picture')[0]->nodeValue : '');
            $category_id = $row->getElementsByTagName('categoryId')[0]->nodeValue;
            $currency_id = $row->getElementsByTagName('currencyId')[0]->nodeValue;
            $price = $row->getElementsByTagName('price')[0]->nodeValue;
            $oldprice = $row->getElementsByTagName('oldprice')[0]->nodeValue;
            $vendor = $row->getElementsByTagName('vendor')[0]->nodeValue;
            Product::query()->create([
                'offer_id' => $offer_id,
                'name' => $name,
                'url' => $url,
                'parent_id' => $parentId,
                'picture' => $picture,
                'category_id' => $category_id,
                'currency_id' => $currency_id,
                'price' => $price,
                'oldprice' => $oldprice,
                'vendor' => $vendor,
                'available' => $available
            ]);
        }
        return true;
    }
}

