<?php
namespace App\Http\Controllers;

use App\Services\XmlParseService;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Services\ExcelGenerator;
use Illuminate\Support\Facades\DB;

class TableExcelController extends Controller
{

    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
    {
        $files = scandir(public_path('/download/'), SCANDIR_SORT_DESCENDING);
        if (! empty($files) && $files[0] != '..') {
            $xls = $files[0];
            $countCategory = Category::all('id')->count();
            $countProducts = Product::all('id')->count();
            return view('table_excel', [
                'downlink' => $xls,
                'countCategory' => $countCategory,
                'countProducts' => $countProducts,
                'panel' => true
            ]);
        }
        return view('table_excel', [
            'panel' => false
        ]);
    }

    public function upload(Request $request): mixed
    {
        $file = $request->file('xmlfile');
        $xmlParser = new XmlParseService();
        try {
            $result = $xmlParser->parseAndSave($file->getContent());
            if (! $result) {
                return abort(200, 'Ошибка обработки XML документа');
            }
            $countCategory = Category::all('id')->count();
            $countProducts = Product::all('id')->count();
            $excelGenerator = new ExcelGenerator();
            $xls = $excelGenerator->saveExcel();
            if (empty($xls)) {
                return abort(200, 'Ошибка генерации Excel-файла');
            }
            return view('table_excel', [
                'downlink' => $xls,
                'countCategory' => $countCategory,
                'countProducts' => $countProducts,
                'panel' => true
            ]);
        } catch (\Exception $e) {
            return abort(200, 'Ошибка загрузки XML документа! ' . $e->getMessage());
        }
    }

    public function panel(): \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
    {
        $products = Product::getProducts();
        if (! $products->count()) {
            $products = null;
        }
        return view('panel',['data' => $products]);
    }
}
