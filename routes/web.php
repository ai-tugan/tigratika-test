<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TableExcelController;

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider and all of them will
 * | be assigned to the "web" middleware group. Make something great!
 * |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tablegen', [TableExcelController::class, 'index']);

Route::post('/tablegen', [
    TableExcelController::class,
    'upload'
]);

Route::get('/tablegen/panel', [TableExcelController::class, 'panel']);
